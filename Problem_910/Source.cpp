
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int *binary_search_smaller(int *start, int *end, int a) {
  int range;
  int *middle;

  while (true) {
    range = (end - start);
    middle = start + (range >> 1);

    if (range == 1)
      return start;
    else if (*middle == a)
      return middle;
    else if (*middle < a) {
      start = middle;
      continue;
    }
    else {
      end = middle;
      continue;
    }
  }
}

int *binary_search_closest(int *start, int *end, int a) {
  int *smaller = binary_search_smaller(start, end, a);

  if (smaller + 1 < end)
    return a - *smaller <= *(smaller + 1) - a ? smaller : smaller + 1;
  else
    return smaller;
}

int main() {
  int N, K;
  int *n_arr, *k_arr;

  cin >> N >> K;

  n_arr = new int[N];
  for (int i = 0; i < N; i++)
    cin >> n_arr[i];

  k_arr = new int[K];
  for (int i = 0; i < K; i++)
    cin >> k_arr[i];

  for (int i = 0; i < K; i++)
    cout << *binary_search_closest(n_arr, n_arr + N, k_arr[i]) << endl;
  delete[] n_arr;
  delete[] k_arr;

  return 0;
}